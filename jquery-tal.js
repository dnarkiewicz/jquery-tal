(function ($) {

	var template_attributes = [];

	function render( node, template_context )
	{
		template_context = template_context || {};
		var $rendered = null;
		if ( $(node).prop('tagName').toLowerCase() == 'script' )// && $(node).attr('src') )
		{
			$rendered = $('<div/>');
			$rendered.append( $.parseHTML($.trim($(node).html())) );
		} else {
			$rendered = $(node).clone(true,true);
		}
		var context = newContext( template_context );
        /// evaluation needs to be smarter
        /// evaluate define/omit not in a loop before the loops
        /// wait to evaluate anything inside a loop until the loop is evaluated
        /// $('[attr]').not('[loop] [attr]') - but this will fail when evaluating a loop's children
		for ( var a in template_attributes )
		{
			var attribute = template_attributes[a];
			for ( var name in attribute.attrs )
			{
				var attribute_name = attribute.attrs[name];
				var selector = '['+attribute_name.replace(':','\\:')+']';
				try {
					//$rendered.find(selector).not('[data-foreach] '+selector).not('[tal\\:repeat] '+selector).addBack(selector).each(function(){
					$rendered.find(selector).addBack(selector).each(function(){
						context.node = this;
						attribute.handler( context, $(this).attr(attribute_name), attribute_name, selector );
					});
				} catch(err) {
					console.error('jquery-tal:render:'+attribute_name+':'+err);
					return $('<div>error</div>');
				}
			}
		}
		return $rendered.attr('id','');
	}


	function renderRemove( context, attribute_value, attribute_name, selector )
	{
		$(context.node).remove();
	}
	function renderConditional( context, attribute_value, attribute_name, selector )
	{
		var condition = resolve( attribute_value, this_context );
		if ( typeof condition != 'undefined' || condition )
		{
			$(context.node).removeAttr(attribute_name);
		} else {
			$(context.node).remove();
		}
	}
	function renderLoop( context, attribute_value, attribute_name, selector )
	{
		var loop_context = newLoopContext(context);
		var list = resolveForeach( attribute_value, loop_context );
		var arr  = [];
		if ( Array.isArray(list) )
		{
			arr = list;
		} else if ( list.hasOwnProperty ) {
			for( var k in list )
			{
				if ( list.hasOwnProperty(k) )
				{
					arr.push(list[k]);
				}
			}
		}
		loop_context.reset(arr.length);
		$(loop_context.node).removeAttr(attribute_name);
		for ( var i=0; i<arr.length; i++ )
		{
			loop_context.item = arr[i];
			loop_context[loop_context.key] = loop_context.item;
			var $new_node = $(context.node)
								.clone(true,true)
			                  	.removeAttr(attribute_name)
				     	      	.insertBefore($(context.node));
			var id = $new_node.attr('id');
			if ( id )
			{
				$new_node.attr('id',id+'_'+loop_context.number)
			}
			$new_node.replaceWith( render($new_node,loop_context) );
			loop_context.iterate();
		}
		$(context.node).remove();
	}
	function renderDefine( context, attribute_value, attribute_name, selector )
	{
		resolve( attribute_value, context );
	}
	function renderContent( context, attribute_value, attribute_name, selector )
	{
		var this_context     = newContext(context);
		this_context.content = $(this_context.node).html();
		var new_content      = resolve( attribute_value, this_context );
		if ( typeof new_content == 'undefined' )
		{
			$(context.node).html('');
		} else {
			$(context.node).html(new_content);
		}
		$(context.node).removeAttr(attribute_name);
	}
	function renderReplace( context, attribute_value, attribute_name, selector )
	{
		var this_context  = newContext(context);
		var replacement   = resolve( attribute_value, this_context );
		if ( typeof replacement != 'undefined' )
		{
			$(context.node).after(replacement);
			$(context.node).remove();
		}
	}
	function renderAttributes( context, attribute_value, attribute_name, selector )
	{
		var this_context   = newContext(context);
		var new_attributes = resolveAttributes( attribute_value, this_context );
		for ( var a in new_attributes )
		{
			$(context.node).attr(a,new_attributes[a]);
		}
	}

	function resolve( js, resolution_context ) 
	{
		try 
		{
			with ( resolution_context )
			{
				return eval(js);
			}
		} catch (err) {
			return js;
		}
	}

	function resolveAttributes( js, resolution_context ) 
	{
		var attrs   = {};
		var __attrs = $(resolution_context.node)[0].attributes;
		for ( var i=0; i<__attrs.length; i++ )
		{
			attrs[__attrs.item(i).name] = __attrs.item(i).value;
		}
		try 
		{
			with ( resolution_context )
			{
				eval(js);
			}
			return attrs;
		} catch (err) {
			return attrs;
		}
	}
	function resolveForeach( js, resolution_context ) 
	{
		var find_iterator = /^\s*(?:(?:var\s+)?([^\s]+)\s+(?:in\s*)?)?(.*)\s*$/;
		var listVar = null;
		var parts   = find_iterator.exec(js);
		if ( parts[2] )
		{
			listVar = parts[2];
		} else {
			return [];
		}
		resolution_context.key = parts[1] || 'item';
		try {
			var list = resolve(listVar,resolution_context);
			return list;
		} catch(err) {
			return [];
		}
	}

	function newContext( base_context )
	{
		var SubContext = function(){};
		//SubContext.id = (Math.random()*10000)+1000;
		SubContext.prototype = base_context;
		return new SubContext();
	}
	function newLoopContext( base_context )
	{
		var loop = newContext(base_context);

		loop.index  = -1;
		loop.number = 0;
        loop.start  = false;
        loop.end    = false;
        loop.even   = false; 
        loop.odd    = false; 

		loop.key    = null;
        loop.length = 0;

        loop.reset   = function( length ) {
        	this.length = length || 0;
        	this.index = -1;
        	this.iterate();
        }
        loop.iterate = function() {
        	this.index++;
        	this.number = (this.index+1);
        	this.start  = (this.index==0);
        	this.end    = (this.index>=(this.length-1));
        	this.even   = (this.index%2==0);
        	this.odd    = (!this.even);
        }
     	return loop;
	}

	function attr( attributes, handler )
	{
		if ( ! Array.isArray(attributes) )
		{
			attributes = [ attributes ];
		}
		template_attributes.push( {attrs:attributes, handler:handler } );
	}


	function tal ( template_source, data )
	{
		var $rendered = render( template_source, data );
		$(this).html( $.parseHTML($.trim($rendered.html())) );
	}

	/// setup default handlers 

	attr( [ 'tal:repeat',     'data-foreach' ], renderLoop     );
	attr( [ 'tal:omit-tag',   'data-omit'    ], renderRemove      );
	attr( [ 'tal:define',     'data-js'      ], renderDefine      );
	attr( [ 'tal:condition',  'data-if'      ], renderConditional );
	attr( [ 'tal:content',    'data-content' ], renderContent     );
	attr( [ 'tal:replace',    'data-replace' ], renderReplace     );
	attr( [ 'tal:attributes', 'data-attrs'   ], renderAttributes  );

	$.talAttr = attr;
	$.fn.tal  = tal;

})(jQuery);